<?php

namespace PHPStamp\Document;

use PHPStamp\Document\WordDocument\Cleanup;
use PHPStamp\Exception\InvalidArgumentException;
use PHPStamp\Processor\Tag;
use PHPStamp\Document\WordDocument\Extension\Nth;
use PHPStamp\Document\WordDocument\Extension\ListItem;
use PHPStamp\Document\WordDocument\Extension\Cell;

/**
 * @link https://docs.microsoft.com/en-us/office/open-xml/working-with-wordprocessingml-documents
 */
class WordDocument extends Document
{
    private const STRUCTURE = [
        Document::XPATH_PARAGRAPH => 'w:p',
        Document::XPATH_RUN => 'w:r',
        Document::XPATH_RUN_PROPERTY => 'w:rPr',
        Document::XPATH_TEXT => 'w:t'
    ];

    /**
     * @inherit
     */
    public function getContentPath()
    {
        return 'word/document.xml';
    }

    /**
     * @inherit
     */
    public function getNodeName($type, $global = false)
    {
        if (isset(self::STRUCTURE[$type]) === false) {
            throw new InvalidArgumentException('Element with this index not defined in structure');
        }

        $return = [];
        if ($global === true) {
            $return[] = '//';
        }
        $return[] = self::STRUCTURE[$type];

        return implode($return);
    }

    /**
     * @inherit
     */
    public function getNodePath()
    {
        return '//' . self::STRUCTURE[Document::XPATH_TEXT];
    }

    /**
     * @inherit
     */
    public function cleanup(\DOMDocument $template)
    {
        // fix node breaks
        $cleaner = new Cleanup(
            $template,
            $this->getNodeName(Document::XPATH_PARAGRAPH, true),
            $this->getNodeName(Document::XPATH_RUN),
            $this->getNodeName(Document::XPATH_RUN_PROPERTY),
            $this->getNodeName(Document::XPATH_TEXT)
        );

        $cleaner->hardcoreCleanup();
        $cleaner->cleanup();
    }

    /**
     * @inherit
     */
    public function getExpression($id, Tag $tag)
    {
        $available = [
            'cell' => Cell::class,
            'listitem' => ListItem::class,
            'nth' => Nth::class,
        ];

        if (isset($available[$id]) === false) {
            throw new InvalidArgumentException('Class by id "' . $id . '" not found.');
        }

        $className = $available[$id];

        return new $className($tag);
    }
}
