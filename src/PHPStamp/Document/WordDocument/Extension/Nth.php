<?php

namespace PHPStamp\Document\WordDocument\Extension;

use PHPStamp\Exception\ExtensionException;
use PHPStamp\Extension\Extension;
use PHPStamp\Processor;

class Nth extends Extension
{
    /**
     * @inherit
     * @throws ExtensionException
     */
    protected function prepareArguments(array $arguments)
    {
        if (count($arguments) !== 2) {
            throw new ExtensionException('Wrong arguments number, 2 needed, got ' . count($arguments));
        }

        return $arguments;
    }

    /**
     * @inherit
     */
    protected function insertTemplateLogic(array $arguments, \DOMElement $node)
    {
        [$rowName, $indexPlusOne] = $arguments;
        $index = $indexPlusOne - 1;

        $xmlPath = $this->tag->getXmlPath();
        $textContent = $this->tag->getTextContent();

        $absolutePath = '/' . Processor::VALUE_NODE . '/' . $rowName . '_' . $index . '_' . $xmlPath;
        Processor::insertTemplateLogic($textContent, $absolutePath, $node);
    }
}
