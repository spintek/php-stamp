<?php

namespace PHPStamp\Document\WordDocument\Extension;

use PHPStamp\Exception\ExtensionException;
use PHPStamp\Extension\Extension;
use PHPStamp\Processor;
use PHPStamp\XMLHelper;

class Cell extends Extension
{
    /**
     * @inherit
     */
    protected function prepareArguments(array $arguments)
    {
        $argumentCount = \count($arguments);
        if ($argumentCount < 1 || $argumentCount > 2) {
            throw new ExtensionException('Wrong arguments number, 1..2 needed, got ' . $argumentCount);
        }

        return $arguments;
    }

    /**
     * @inherit
     */
    protected function insertTemplateLogic(array $arguments, \DOMElement $node)
    {
        $rowNameBase = $arguments[0];
        $start = $arguments[1] ?? 1;
        $rowName = $rowNameBase . ($start > 1 ? '_' . $start : '');
        $nextRowSelectors = [':cell(' . $rowNameBase . ',' . $start . ')'];
        if ($start === 1) {
            $nextRowSelectors[] = ':cell(' . $rowNameBase . ')';
        }

        $template = $node->ownerDocument;

        // find existing or initiate new table row template
        if ($this->isRowTemplateExist($rowName, $template) === false) {
            $rowTemplate = $template->createElementNS(Processor::XSL_NS, 'xsl:template');
            $rowTemplate->setAttribute('name', $rowName);

            // find row node
            $rowNode = XMLHelper::parentUntil('w:tr', $node);
            // include following rows if they contain cell variables of the same type
            $nextRowNodes = [];
            $iterator = $rowNode;
            while ($iterator = $iterator->nextSibling) {
                $includeInTemplate = false;
                foreach ($nextRowSelectors as $nextRowSelector) {
                    if (\strpos($iterator->textContent, $nextRowSelector) !== false) {
                        $nextRowNodes[] = $iterator;
                        $includeInTemplate = true;
                        break;
                    }
                }
                if (!$includeInTemplate) {
                    break;
                }
            }

            // call-template for each row
            $foreachNode = $template->createElementNS(Processor::XSL_NS, 'xsl:for-each');
            $foreachNode->setAttribute('select', '/' . Processor::VALUE_NODE . '/' . $rowName . '/item');
            $callTemplateNode = $template->createElementNS(Processor::XSL_NS, 'xsl:call-template');
            $callTemplateNode->setAttribute('name', $rowName);
            $foreachNode->appendChild($callTemplateNode);

            // insert call-template before moving
            $rowNode->parentNode->insertBefore($foreachNode, $rowNode);

            // move node into row template
            $rowTemplate->appendChild($rowNode);
            foreach ($nextRowNodes as $nextRowNode) {
                $rowTemplate->appendChild($nextRowNode);
            }
            $template->documentElement->appendChild($rowTemplate);
        }

        $relativePath = $this->tag->getRelativePath();
        Processor::insertTemplateLogic($this->tag->getTextContent(), $relativePath, $node);
    }

    private function isRowTemplateExist($rowName, \DOMDocument $template)
    {
        $xpath = new \DOMXPath($template);
        $nodeList = $xpath->query('/xsl:stylesheet/xsl:template[@name="' . $rowName . '"]');

        if ($nodeList->length > 1) {
            throw new ExtensionException('Unexpected template count.');
        }

        return ($nodeList->length === 1);
    }
}
